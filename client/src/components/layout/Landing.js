import React, { Component } from "react";
import { Link } from "react-router-dom";
import { lory } from 'lory.js';
import "./Landing.css";

function IntroImages(props) {
  return (
    <div className="col-small-4 col-medium-2 col-4 u-align--center">
      <div>
        <img src={props.src} loading="eager" width="200" height="200" />
      </div>
      <div className="main-introduction-image-desc">gut</div>
    </div>
  );
}

class Slider extends React.Component {
  constructor(props) {
    super(props);
    this.myRef = React.createRef();
  }

  componentDidMount() {
    this.timerID = setInterval(() => this.tick(), 6000);
    this.instance = lory(this.myRef.current, { infinite: 1 });
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  tick() {
    this.instance.next();
  }

  render() {
    return (
      <div id="main-content-img" className="slider js_slider" ref={this.myRef}>
        <div className="frame js_frame">
          <ul className="slides js_slides">
            <li class="js_slide">
              <img src="https://dummyimage.com/600x360/344/fff" />
            </li>
            <li class="js_slide">
              <img src="https://dummyimage.com/600x360/954/fff" />
            </li>
            <li class="js_slide">
              <img src="https://dummyimage.com/600x360/336/fff" />
            </li>
          </ul>
        </div>
      </div>
    );
  }
}

function Landing(props) {
  return (
    <div>
      <div
        id="main-content"
        className="p-strip--image is-deep"
      >
        <div
          className="u-fixed-width"
        >
          <Slider />
          <div id="main-content-desc">
            one two three four five six seven eight nine ten up down strange charm
          </div>
        </div>
      </div>

      <section id="main-introduction" className="p-strip">
        <div className="u-fixed-width">
          <h2 className="p-heading--two u-align--center">Our Space</h2>
          <p>
            one two three four five six seven eight nine ten up down strange
            charm top bottom red green blue antired antigreen antiblue
          </p>
        </div>

        <div id="main-introduction-images-row" className="row">
          <IntroImages src="https://dummyimage.com/200x200/acc/fff" />
          <IntroImages src="https://dummyimage.com/200x200/acc/fff" />
          <IntroImages src="https://dummyimage.com/200x200/acc/fff" />
        </div>
      </section>

      <section id="landing-pricing" className="p-strip">
        <div className="u-fixed-width">
          <h2 className="p-heading--two u-align--center">Pricing</h2>
        </div>

        <div className="u-fixed-width u-align--center">
          <img src="https://dummyimage.com/500x300/bba/fff" />
        </div>
      </section>

      <footer class="p-strip--light p-sticky-footer" id="footer">
        <div class="row">
          <div class="col-4">
            <p>Address: flip flop</p>
            <p>Opening Hour: ping pong</p>
          </div>
          <div class="col-5">
            <p>Email: moshi moshi</p>
            <p>Hotline: 1823</p>
          </div>
          <div class="col-3">
             <ul class="p-inline-list u-align--right">
               <li class="p-inline-list__item">
                 <a href="https://twitter.com/snapcraftio" class="p-icon--twitter">Share on Twitter</a>
               </li>
               <li class="p-inline-list__item">
                 <a href="https://www.facebook.com/snapcraftio" class="p-icon--facebook">Share on Facebook</a>
               </li>
               <li class="p-inline-list__item">
                 <a href="https://www.youtube.com/snapcraftio" class="p-icon--youtube">Share on YouTube</a>
               </li>
             </ul>
           </div>
        </div>
      </footer>

    </div>
  );
}

export default Landing;

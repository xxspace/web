import React, { Component } from "react";
import { Link } from "react-router-dom";

class Navbar extends Component {
  render() {
    return (
      <header id="navigation" className="p-navigation">
        <div className="p-navigation__row">
          <div className="p-navigation__banner">
            <div className="p-navigation__logo">
              <a className="p-navigation__item" href="#">
                {/*
                <img className="p-navigation__image" src="https://assets.ubuntu.com/v1/5d6da5c4-logo-canonical-aubergine.svg" alt="" width="95"/>
                */}
                <div className="p-navigation__link" href="/">
                  <span style={{ fontWeight: "bold", color: "black" }}>xxxspace</span>
                </div>
              </a>
            </div>
            <a href="#navigation" className="p-navigation__toggle--open" title="menu">Menu</a>
            <a href="#navigation-closed" className="p-navigation__toggle--close" title="close menu">Close menu</a>
          </div>
          <nav className="p-navigation__nav">
            <span className="u-off-screen">
              <a href="#main-content">Jump to main content</a>
            </span>
            <ul className="p-navigation__items" role="menu">
              <li className="p-navigation__item is-selected" role="menuitem">
                <a className="p-navigation__link" href="/">Home</a>
              </li>
              <li className="p-navigation__item" role="menuitem">
                <a className="p-navigation__link" href="#">About Us</a>
              </li>
              <li className="p-navigation__item" role="menuitem">
                <a className="p-navigation__link" href="#">Pricing</a>
              </li>
              <li className="p-navigation__link" role="menuitem">
                <a className="p-navigation__link" href="#">Get a Apot</a>
              </li>
              <li className="p-navigation__link" role="menuitem">
                <a className="p-navigation__link" href="#">Find Us</a>
              </li>
            </ul>

            <ul className="p-navigation__items" role="menu">
              <li className="p-navigation__link" role="menuitem">
                <Link to="/register">
                  Register
                </Link>
              </li>
              <li className="p-navigation__link" role="menuitem">
                <Link to="/login">
                  Log In
                </Link>
              </li>
            </ul>
          </nav>
        </div>
      </header>
    );
  }
}

export default Navbar;
